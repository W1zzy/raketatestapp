//
//  Configurable.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 05.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol Configurable {
    /// Type of model that is being transferred
    associatedtype ModelType

    /// Updates view with `model`.
    func configure(with model: ModelType)

}
