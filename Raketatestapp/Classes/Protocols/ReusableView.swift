//
//  ReusableView.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 05.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

protocol ReusableView {
    static func reuseIdentifier() -> String
}

extension ReusableView {
    static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}

// MARK: - UITableViewCell

extension ReusableView where Self: UITableViewCell {
    static func registerFor(tableView: UITableView) {
        tableView.register(nib, forCellReuseIdentifier: Self.reuseIdentifier())
    }
}

extension UITableViewCell: ReusableView {}
