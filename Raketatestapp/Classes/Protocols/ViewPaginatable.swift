//
//  ViewPaginatable.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

import UIKit

enum PaginationType {

    case horizontal
    case vertical

}

protocol ViewPaginatable where Self: UIScrollViewDelegate {

    var infinityScrollBlocked: Bool { get set }
    var paginationType: PaginationType { get }

    func scrollViewDidScrollOverThreshold()

}

extension ViewPaginatable {

    func scrollViewScrolled(_ scrollView: UIScrollView) {
        guard !infinityScrollBlocked else { return }
        let contentSize = paginationType == .vertical ? scrollView.contentSize.height : scrollView.contentSize.width
        if contentSize > 0 {
            let scrollViewSize = paginationType == .vertical ? scrollView.bounds.size.height : scrollView.bounds.size.width
            let currentOffsetPosition = paginationType == .vertical ? scrollView.contentOffset.y : scrollView.contentOffset.x

            // 30% threshold
            if (contentSize - currentOffsetPosition < 0.3 + scrollViewSize) {
                scrollViewDidScrollOverThreshold()
            }
        }
    }

}
