//
//  RefreshControl.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

typealias RefreshCompletion = () -> Void

protocol RefreshControlPresenting: class {

    func refresh(with completion: @escaping RefreshCompletion)

}

final class RefreshControl: UIRefreshControl {

    private weak var presenter: RefreshControlPresenting?

    init(presenter: RefreshControlPresenting) {
        self.presenter = presenter
        super.init()
        addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        applyTheme()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func valueChanged() {
        presenter?.refresh(with: { [weak self] in
            if self?.isRefreshing == true {
                self?.endRefreshing()
            }
        })
    }

    private func applyTheme() {
        tintColor = .white
    }

}
