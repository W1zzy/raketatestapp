//
//  UIViewController+Init.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

extension UIViewController: DefaultIdentifier {

    static func instantiate <T: UIViewController>(with stroryboardName: StoryboardNames) -> T {
        guard let controller = UIStoryboard(name: stroryboardName.rawValue, bundle:Bundle.main).instantiateViewController(withIdentifier: T.defaultIdentifier) as? T else {
            fatalError("Could not dequeue controller with identifier: \(T.defaultIdentifier) in storyboard: \(stroryboardName)")
        }
        return controller
    }

}
