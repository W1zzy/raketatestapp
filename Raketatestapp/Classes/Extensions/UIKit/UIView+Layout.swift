//
//  UIView+Layout.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 05.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

// MARK: - Layout

extension UIView {
    func pinViewToEdgesOfSuperview(leftOffset: CGFloat = 0, rightOffset: CGFloat = 0, topOffset: CGFloat = 0, bottomOffset: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        superview!.addConstraints([
            superview!.leftAnchor.constraint(equalTo: leftAnchor, constant: leftOffset),
            superview!.rightAnchor.constraint(equalTo: rightAnchor, constant: rightOffset),
            superview!.topAnchor.constraint(equalTo: topAnchor, constant: topOffset),
            superview!.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomOffset)
            ])
    }

    func pinViewToCenterOfSuperview(offsetX: CGFloat = 0.0, offsetY: CGFloat = 0.0) {
        translatesAutoresizingMaskIntoConstraints = false
        superview!.addConstraints([
            centerXAnchor.constraint(equalTo: superview!.centerXAnchor, constant: offsetX),
            centerYAnchor.constraint(equalTo: superview!.centerYAnchor, constant: offsetY)
            ])
    }

    func pinViewWidthAndHeight(width: CGFloat, height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        superview!.addConstraints([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: height)
            ])
    }

    func pinViewToBottomOfSuperview(height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        superview!.addConstraints([
            heightAnchor.constraint(equalToConstant: height),
            superview!.leftAnchor.constraint(equalTo: leftAnchor),
            superview!.rightAnchor.constraint(equalTo: rightAnchor),
            superview!.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }

    func setViewToEdgesOfSuperview(leftOffset: CGFloat = 0, rightOffset: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        superview!.addConstraints([
            superview!.leftAnchor.constraint(equalTo: leftAnchor, constant: leftOffset),
            superview!.rightAnchor.constraint(equalTo: rightAnchor, constant: rightOffset)
            ])
    }
}
