//
//  UIActivityIndicatorView+Helpers.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 06.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView {

    enum ActivityIndicatorType {
        case white, gray
    }

    static func buildActivityIndicator(with type: ActivityIndicatorType = .gray) -> UIActivityIndicatorView {
        let activityIndicator: UIActivityIndicatorView
        if #available(iOS 13, *) {
            activityIndicator = UIActivityIndicatorView(style: .large)
        } else {
            activityIndicator = UIActivityIndicatorView(style: type == .white ? .white : .gray)
        }
        if type == .white {
            activityIndicator.color = .white
        }
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        return activityIndicator
    }

    func centerOnView(_ view: UIView) {
        view.addSubview(self)
        self.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.bringSubviewToFront(self)
    }

}
