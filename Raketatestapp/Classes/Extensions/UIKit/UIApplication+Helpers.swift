//
//  UIApplication+Helpers.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

extension UIApplication {

    static func openURL(_ url: URL?) {
        if let url = url, shared.canOpenURL(url) {
            shared.open(url)
        }
    }

}
