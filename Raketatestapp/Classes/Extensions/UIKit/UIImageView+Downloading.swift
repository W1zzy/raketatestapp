//
//  UIImageView+Downloading.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 06.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

extension UIImageView {

    func setImage(with url: URL) {
        let activityIndicator = UIActivityIndicatorView.buildActivityIndicator(with: .gray)
        addSubview(activityIndicator)
        activityIndicator.centerOnView(self)

        activityIndicator.startAnimating()
        Downloader.shared.downloadRequest(url) { [weak self] result in
            activityIndicator.stopAnimating()
            if case .success(let data) = result {
                self?.image = UIImage(data: data)
            }
        }
    }

}
