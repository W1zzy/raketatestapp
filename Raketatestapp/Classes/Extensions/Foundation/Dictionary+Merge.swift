//
//  Dictionary+Merge.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

func += <K, V> (left: inout [K: V], right: [K: V]) {
    for (k, v) in right {
        left[k] = v
    }
}
