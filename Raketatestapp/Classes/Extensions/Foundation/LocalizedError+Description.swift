//
//  LocalizedError+Description.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

extension LocalizedError where Self: CustomStringConvertible {

   var errorDescription: String? {
      return description
   }
}
