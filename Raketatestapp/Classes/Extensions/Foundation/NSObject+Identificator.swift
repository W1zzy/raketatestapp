//
//  NSObject+Identificator.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol DefaultIdentifier: class {

    static var defaultIdentifier: String { get }

}

extension DefaultIdentifier where Self: NSObject {

    static var defaultIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }

}

extension NSObject {

    static var className: String {
        return String(describing: self)
    }

    var className: String {
        return String(describing: self)
    }

}
