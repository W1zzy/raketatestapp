//
//  FileUtility.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 06.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol DataCaching {

    func write(data: Data, name: String?) -> URL?
    func read(dataWith name: String) -> Data?
    func fileURL(for name: String) -> URL?
    func remove(dataWith name: String) -> Bool
    func removeAll() -> Bool

}

final class FileUtility: DataCaching {

    @discardableResult
    func write(data: Data, name: String? = nil) -> URL? {
        guard createDocumentsFolderIfNeeded() else { return nil }

        let fileURL: URL?
        do {
            let url = self.url(for: name)
            try data.write(to: url)
            fileURL = url
        } catch {
            fileURL = nil
        }

        return fileURL
    }

    func read(dataWith name: String) -> Data? {
        let readData: Data?
        do {
            readData = try Data(contentsOf: url(for: name) as URL)
        } catch {
            readData = nil
        }
        return readData
    }

    func fileURL(for name: String) -> URL? {
        let fileURL = documentsDirectory.appendingPathComponent(name)
        let isFileExist = FileManager.default.fileExists(atPath: fileURL.path)
        return isFileExist ? fileURL : nil
    }

    @discardableResult
    func remove(dataWith name: String) -> Bool {
        let url = self.url(for: name)
        let wasRemoved: Bool
        do {
            try FileManager.default.removeItem(at: url)
            wasRemoved = true
        } catch {
            wasRemoved = false
        }
        return wasRemoved
    }

    @discardableResult
    func removeAll() -> Bool {
        let wereRemoved: Bool
        do {
            try FileManager.default.removeItem(at: documentsDirectory)
            wereRemoved = true
        } catch {
            wereRemoved = false
        }
        return wereRemoved
    }

}

private extension FileUtility {

    @discardableResult
    func createDocumentsFolderIfNeeded() -> Bool {
        let path = documentsDirectory.path
        guard !FileManager.default.fileExists(atPath: path) else { return true }
        let wasCreated: Bool
        do {
            try FileManager.default.createDirectory(atPath: path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            wasCreated = true
        } catch {
            wasCreated = false
        }
        return wasCreated
    }

    func url(for name: String?) -> URL {
        let resultName = name ?? UUID().uuidString
        let url = documentsDirectory.appendingPathComponent(resultName)
        return url
    }

}

private extension FileUtility {

    struct Constant {
        static let documentPath = "Documents"
    }

    var documentsDirectory: URL {
        guard let documentsPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory,
                                                                      .userDomainMask, true).first else {
            fatalError("Unable to find document directory")
        }
        return URL(fileURLWithPath: documentsPath).appendingPathComponent(Constant.documentPath)
    }

}
