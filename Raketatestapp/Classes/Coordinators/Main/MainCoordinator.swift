//
//  MainCoordinator.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

final class MainCoordinator: Coordinator {

    // MARK: Instance Properties

    var children: [Coordinator] = []
    let router: Router

    private lazy var topList: TopListViewController = .instantiate(with: .topList)
    private let networking: Networking = NetworkService()

    // MARK: Lifecycle

    init(router: Router) {
        self.router = router
    }

    // MARK: Coordinator

    func present(animated: Bool, onDismissed: (() -> Void)?) {
        let assembly = TopListAssembly(coordinator: self,
                                       networking: networking)
        router.present(assembly.assemble().toPresent,
                       animated: animated,
                       onDismissed: onDismissed)
    }

}

extension MainCoordinator: TopListCoordinating {

    func openPreview(for url: URL) {
        let assembly = ImagePreviewAssembly(coordinator: self,
                                            imageURL: url)
        router.show(assembly.assemble().toPresent,
                    animated: true)
    }

}

extension MainCoordinator: ImagePreviewCoordinating {

    func close() {
        router.hide(animated: true)
    }

}
