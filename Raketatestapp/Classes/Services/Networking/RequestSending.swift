//
//  RequestSending.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

enum ServiceError: LocalizedError, CustomStringConvertible {
    case invalidRequest
    case invalidProcessData

    var description: String {
        switch self {
        case .invalidRequest:
            return "Invalid request"
        case .invalidProcessData:
            return "Invalid data received"
        }
    }
}

protocol RequestSending {

    func dataTask<T: Decodable>(with request: URLRequest,
                                session: URLSession,
                                response: T.Type,
                                completion: @escaping NetworkHandler<T>)

}

extension RequestSending {

    func dataTask<T: Decodable>(with request: URLRequest,
                                session: URLSession,
                                response: T.Type,
                                completion: @escaping NetworkHandler<T>) {
        session.dataTask(with: request) { data, httpResponse, error in
            // Main handling
            func mainCompletion<A>(_ completion: @escaping NetworkHandler<A>, result: NetworkResult<A>) {
                DispatchQueue.main.async {
                    completion(result)
                }
            }

            // Check client error
            if let error = error {
                return mainCompletion(completion, result: .failure(error))
            }

            // Check server error
            guard let httpResponse = httpResponse as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    return mainCompletion(completion, result: .failure(ServiceError.invalidProcessData))
            }

            guard let data = data else {
                return mainCompletion(completion, result: .failure(ServiceError.invalidProcessData))
            }

            // Get the value from the body, could be change of decoding factory, for test
            // left like it is, assume that we will work only with JSON data for now
            guard let value = try? JSONDecoder().decode(T.self, from: data) else {
                return mainCompletion(completion, result: .failure(ServiceError.invalidProcessData))
            }

            // Finished with result
            mainCompletion(completion, result: .success(value))
        }.resume()
    }

}
