//
//  ParameterEncoding.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

enum ParameterEncoding: String {

    case json
    case urlEncoded

}

extension ParameterEncoding {

    var httpHeader: String {
        switch self {
        case .json :
            return "application/json"
        case .urlEncoded :
            return "application/x-www-form-urlencoded"
        }
    }

    var codingHeaders: HTTPHeaders {
        return ["Content-Type": httpHeader]
    }

}
