//
//  Networking.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

/// Defines network result with type T
typealias NetworkResult<T> = Result<T, Error>

/// Defines result of the network requests with type T
typealias NetworkHandler<T> = (NetworkResult<T>) -> Void

/**
 Represent abstraction for sending requests and processing responses of type T
 */
protocol Networking {

    func sendRequest<T: Decodable, R: Endpoint>(_ request: R,
                                                response: T.Type,
                                                completion: @escaping NetworkHandler<T>)

}

class NetworkService: Networking, RequestSending {

    /// Service session
    private let urlSession: URLSession

    // MARK: Initialization

    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }

    // MARK: Networking

    func sendRequest<T: Decodable, R: Endpoint>(_ request: R,
                                                response: T.Type,
                                                completion: @escaping NetworkHandler<T>) {
        guard let urlRequest = request.makeURLRequest() else {
            completion(.failure(ServiceError.invalidRequest))
            return
        }

        dataTask(with: urlRequest,
                 session: urlSession,
                 response: response,
                 completion: completion)
    }

}
