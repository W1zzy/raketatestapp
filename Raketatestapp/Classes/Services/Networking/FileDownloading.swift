//
//  FileDownloading.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 06.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol FileDownloading {

    func downloadTask(with request: URLRequest,
                      session: URLSession,
                      completion: @escaping FileDownloadHandler)

}

extension FileDownloading {

    func downloadTask(with request: URLRequest,
                      session: URLSession,
                      completion: @escaping FileDownloadHandler) {
        guard let fileName = request.url?.lastPathComponent else {
            completion(.failure(ServiceError.invalidRequest))
            return
        }

        let fileUtility = FileUtility()

        if let localFileURL = fileUtility.fileURL(for: fileName),
           let data = try? Data(contentsOf: localFileURL) {
            completion(.success(data))
        } else {
            session.downloadTask(with: request) { tempUrl, httpResponse, error in
                func mainCompletion(_ completion: @escaping FileDownloadHandler, result: NetworkResult<Data>) {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }

                if let error = error {
                    mainCompletion(completion,
                                   result: .failure(error))
                    return
                }

                guard let httpResponse = httpResponse as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode) else {
                        mainCompletion(completion,
                                       result: .failure(ServiceError.invalidProcessData))
                        return
                }

                // Temp url shouldn't be empty
                guard let tempUrl = tempUrl else {
                    mainCompletion(completion,
                                   result: .failure(ServiceError.invalidProcessData))
                    return
                }

                guard let data = try? Data(contentsOf: tempUrl) else {
                    // Data from temp url can't be read, file is broken
                    mainCompletion(completion,
                                   result: .failure(ServiceError.invalidProcessData))
                    return
                }

                guard fileUtility.write(data: data, name: fileName) != nil else {
                    mainCompletion(completion,
                                   result: .failure(ServiceError.invalidProcessData))
                    return
                }

                mainCompletion(completion,
                               result: .success(data))
            }.resume()
        }
    }

}
