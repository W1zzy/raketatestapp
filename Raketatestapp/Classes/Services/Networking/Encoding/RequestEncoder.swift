//
//  RequestEncoder.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data?
    func encodeUrlItems(_ parameters: Parameters) -> [URLQueryItem]?

}

extension RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data? { return nil }
    func encodeUrlItems(_ parameters: Parameters) -> [URLQueryItem]? { return nil }

}
