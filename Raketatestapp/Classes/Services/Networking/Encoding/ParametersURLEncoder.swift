//
//  ParametersURLEncoder.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

class ParametersURLEncoder: RequestEncoder {

    func encodeUrlItems(_ parameters: Parameters) -> [URLQueryItem]? {
        return parameters.asURLQueryItems
    }

}

private extension Dictionary where Key == String {

    var asURLQueryItems: [URLQueryItem]? {
        return map({ queryComponents($0, $1) }).reduce([], +)
    }

    private func queryComponents(_ key: String, _ value: Any) -> [URLQueryItem] {
        var components: [URLQueryItem] = []

        if let dictionary = value as? [String: AnyObject] {
            for (nestedKey, value) in dictionary {
                components += queryComponents("\(key)[\(nestedKey)]", value)
            }
        } else if let array = value as? [AnyObject] {
            for value in array {
                components += queryComponents("\(key)", value)
            }
        } else {
            components.append(URLQueryItem(name: key, value: "\(value)"))
        }

        return components
    }

}
