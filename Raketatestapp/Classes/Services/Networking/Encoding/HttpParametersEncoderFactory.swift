//
//  HttpParametersEncoderFactory.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol ParametersEncoderFactory {

    associatedtype Encoder

    static func parametersEncoder(forType codingType: ParameterEncoding) -> Encoder

}

class HttpParametersEncoderFactory: ParametersEncoderFactory {

    static func parametersEncoder(forType codingType: ParameterEncoding) -> RequestEncoder {
        switch codingType {
        case .json:
            return ParametersJSONEncoder()
        case .urlEncoded:
            return ParametersURLEncoder()
        }
    }

}
