//
//  ParametersJSONEncoder.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

class ParametersJSONEncoder: RequestEncoder {

    func encodeBody(_ parameters: Parameters) -> Data? {
        return try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    }

}
