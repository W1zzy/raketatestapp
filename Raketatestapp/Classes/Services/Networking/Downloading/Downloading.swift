//
//  Downloading.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 06.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

/// Define result of the file download
typealias FileDownloadHandler = (NetworkResult<Data>) -> Void

/// Downloader of files, want to make it singleton to use for image download like alamofire or kingsfisher, just in place
class Downloader: FileDownloading {

    static let shared = Downloader()

    private let cache = NSCache<NSString, UIImage>()

    /// Service session
    private let urlSession: URLSession = URLSession.shared

    // MARK: Initialization

    private init() {}

    // MARK: Networking

    func downloadRequest(_ url: URL,
                         completion: @escaping FileDownloadHandler) {
        downloadTask(with: URLRequest(url: url),
                     session: urlSession,
                     completion: completion)
    }

}
