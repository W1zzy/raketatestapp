//
//  Endpoint.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: Any]

protocol Endpoint {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var url: URL? { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }

    func makeURLRequest() -> URLRequest?
}

extension Endpoint {
    var baseURL: String {
        return "https://www.reddit.com/"
    }

    var url: URL? {
        switch self {
        default:
            return URL(string: self.baseURL + self.path)
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        default:
            return .json
        }
    }

    var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"]
        }
    }

    func makeURLRequest() -> URLRequest? {
        guard let baseURL = self.url else { return nil }

        var components = URLComponents()

        components.scheme = baseURL.scheme
        components.host = baseURL.host
        components.port = baseURL.port
        components.path = baseURL.path
        components.queryItems = queryItems

        guard let url = components.url else { return nil }

        var urlRequest = URLRequest(url: url)

        urlRequest.httpMethod = httpMethod.rawValue

        var allHeaders = headers
        allHeaders? += encoding.codingHeaders

        urlRequest.allHTTPHeaderFields = allHeaders
        urlRequest.httpBody = body

        return urlRequest
    }

}

private extension Endpoint {

    var body: Data? {
        guard let parameters = parameters else { return nil }
        return HttpParametersEncoderFactory.parametersEncoder(forType: encoding).encodeBody(parameters)
    }

    var queryItems: [URLQueryItem]? {
        guard let parameters = parameters else { return nil }
        return HttpParametersEncoderFactory.parametersEncoder(forType: encoding).encodeUrlItems(parameters)
    }

}
