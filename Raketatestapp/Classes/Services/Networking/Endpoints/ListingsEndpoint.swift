//
//  ListingsEndpoint.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 03.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

enum ListingsEndpoint {
    case getTop(page: PageModel)
}

extension ListingsEndpoint: Endpoint {

    var path: String {
        switch self {
        case .getTop:
            return "top.json"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }

    var parameters: Parameters? {
        switch self {
        case .getTop(let page):
            return page.dictionary
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        default:
            return .urlEncoded
        }
    }
}
