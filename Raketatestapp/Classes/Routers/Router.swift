//
//  Router.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

protocol Router: class {

    func present(_ viewController: UIViewController, animated: Bool)
    func present(_ viewController: UIViewController,
                 animated: Bool,
                 onDismissed: (()->Void)?)
    func dismiss(animated: Bool)

    func show(_ viewController: UIViewController, animated: Bool)
    func hide(animated: Bool)

}

extension Router {

    func present(_ viewController: UIViewController, animated: Bool) {
        present(viewController, animated: animated, onDismissed: nil)
    }

}
