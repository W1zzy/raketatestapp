//
//  AppDelegateRouter.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class AppDelegateRouter: Router {

    // MARK: Instance Properties

    let window: UIWindow

    // MARK: Object Lifecycle

    init(window: UIWindow) {
        self.window = window
    }

    // MARK: Router

    func present(_ viewController: UIViewController,
                 animated: Bool,
                 onDismissed: (()->Void)?) {
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }

    func dismiss(animated: Bool) { }

    func show(_ viewController: UIViewController, animated: Bool) {
        window.rootViewController?.present(viewController,
                                           animated: animated,
                                           completion: nil)
    }

    func hide(animated: Bool) {
        window.rootViewController?.dismiss(animated: animated, completion: nil)
    }

}
