//
//  PageModel.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

struct PageModel: Codable {

    var after: String?
    let limit: Int

    var isFirst: Bool {
        after == nil
    }

    init(with limit: Int) {
        self.after = nil
        self.limit = limit
    }

    mutating func first() {
        after = nil
    }

}

extension PageModel {

    enum EncodingKeys: String, CodingKey {
        case limit
        case after
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: EncodingKeys.self)
        try container.encode(limit, forKey: .limit)
        try container.encode(after, forKey: .after)
    }

}

extension PageModel {

    enum DecodingKeys: String, CodingKey {
        case dist
        case after
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)

        limit = try container.decode(Int.self, forKey: .dist)
        after = try? container.decode(String.self, forKey: .after)
    }

}
