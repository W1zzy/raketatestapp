//
//  TopListResponseModel.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 05.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

struct TopListResponseModel {

    let page: PageModel
    let items: [TopListItemResponseModel]

}

extension TopListResponseModel: Decodable {

    enum CodingKeys: String, CodingKey {
        case data
        case children
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)

        items = try nestedContainer.decode([TopListItemResponseModel].self, forKey: .children)
        page = try container.decode(PageModel.self, forKey: .data)
    }

}

enum ListItemType: String, Decodable {
    case image
    case link
}

struct TopListItemResponseModel {

    let title: String
    let author: String
    let thumbnail: URL?
    let created: Date
    let commentsCount: Int
    let url: URL?
    let type: ListItemType?

}

extension TopListItemResponseModel: Decodable {

    enum CodingKeys: String, CodingKey {
        case data
        case title
        case author
        case thumbnail
        case created
        case commentsCount = "num_comments"
        case url = "url_overridden_by_dest"
        case type = "post_hint"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)

        title = try nestedContainer.decode(String.self, forKey: .title)
        author = try nestedContainer.decode(String.self, forKey: .author)
        commentsCount = try nestedContainer.decode(Int.self, forKey: .commentsCount)
        type = try? nestedContainer.decode(ListItemType.self, forKey: .type)

        if let stringURL = try? nestedContainer.decode(String.self, forKey: .thumbnail),
            let url = URL(string: stringURL),
            !url.pathExtension.isEmpty {
            self.thumbnail = url
        } else {
            self.thumbnail = nil
        }

        if let stringURL = try? nestedContainer.decode(String.self, forKey: .url),
            let url = URL(string: stringURL),
            !url.pathExtension.isEmpty {
            self.url = url
        } else {
            self.url = nil
        }

        if let timestamp = try? nestedContainer.decode(Int.self, forKey: .created) {
            created = Date(timeIntervalSince1970: TimeInterval(timestamp))
        } else {
            throw DecodingError.dataCorrupted(
              DecodingError.Context(
                codingPath: [CodingKeys.created],
                debugDescription: "Cannot initialize from nil date"
              )
            )
        }

    }

}
