//
//  Assembly.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

/// Describe base assembly functionality
protocol Assembly {

    func assemble() -> View

}
