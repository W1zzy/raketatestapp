//
//  Presenter.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

/// Describe base presenter functionality
protocol Presenter: class {

    func viewLoaded()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()

}

extension Presenter {

    func viewLoaded() {
        assertionFailure("viewLoaded should be handled by presenter")
    }

    func viewWillAppear() {
        assertionFailure("viewWillAppear should be handled by presenter")
    }

    func viewDidAppear() {
        assertionFailure("viewDidAppear should be handled by presenter")
    }

    func viewWillDisappear() {
        assertionFailure("viewWillDisappear should be handled by presenter")
    }

    func viewDidDisappear() {
        assertionFailure("viewDidDisappear should be handled by presenter")
    }

}
