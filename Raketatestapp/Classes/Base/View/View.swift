//
//  View.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

/// Describe base view functionality
/// In our case it could be UIViewController
protocol View: class {

    var toPresent: UIViewController { get }

}

// MARK: Default conformance

extension UIViewController: View {

    var toPresent: UIViewController { self }

}

// MARK: Refresh control

extension View {

    func addRefreshControl(to tableView: UITableView, with presenter: RefreshControlPresenting) {
        tableView.refreshControl = RefreshControl(presenter: presenter)
    }

}

// MARK: UIApplication helpers

extension View {

    func openURL(_ url: URL?) {
        UIApplication.openURL(url)
    }

}

// MARK: Alert showing

protocol AlertPresentable {

    func presentError(_ error: Error)
    func presentInfoAlert(with text: String)

}

extension UIViewController: AlertPresentable {

    func presentError(_ error: Error) {
        presentDefaultAlert(with: "Error",
                            message: error.localizedDescription,
                            action: "Ok")
    }

    func presentInfoAlert(with text: String) {
        presentDefaultAlert(with: "Info",
                            message: text,
                            action: "Ok")
    }

}

private extension AlertPresentable where Self: UIViewController {

    func presentDefaultAlert(with title: String,
                             message: String,
                             action: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: action,
                                      style: .default,
                                      handler: nil))

        present(alert, animated: true, completion: nil)
    }

}
