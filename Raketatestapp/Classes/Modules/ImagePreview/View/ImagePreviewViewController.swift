//
//  ImagePreviewViewController.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

protocol ImagePreviewInput: View {

    func setImageURL(_ imageURL: URL)

}

class ImagePreviewViewController: UIViewController {

    // MARK: Injected

    var presenter: ImagePreviewPresenting?

    // MARK: Outlets

    @IBOutlet private weak var imageView: UIImageView!

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.viewLoaded()
    }

    @IBAction func closeTapped(_ sender: UIBarButtonItem) {
        presenter?.close()
    }

    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        guard let image = imageView.image else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(imageSaved(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    @objc func imageSaved(_ image: UIImage,
                    didFinishSavingWithError error: Error?,
                    contextInfo: UnsafeRawPointer) {
        if let error = error {
            presentError(error)
        } else {
            presentInfoAlert(with: "Image saved")
        }
    }

}

// MARK: ImagePreviewInput

extension ImagePreviewViewController: ImagePreviewInput {

    func setImageURL(_ imageURL: URL) {
        imageView.setImage(with: imageURL)
    }

}
