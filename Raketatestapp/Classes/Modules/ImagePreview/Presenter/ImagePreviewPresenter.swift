//
//  ImagePreviewPresenter.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

protocol ImagePreviewCoordinating: class {

    func close()

}

protocol ImagePreviewPresenting: Presenter {

    func close()

}

class ImagePreviewPresenter {

    // MARK: Injected

    weak var view: ImagePreviewInput?
    private weak var coordinator: ImagePreviewCoordinating?
    private let imageURL: URL

    // MARK: Properties

    init(coordinator: ImagePreviewCoordinating?,
         imageURL: URL) {
        self.coordinator = coordinator
        self.imageURL = imageURL
    }

}

extension ImagePreviewPresenter: ImagePreviewPresenting {

    func viewLoaded() {
        view?.setImageURL(imageURL)
    }

    func close() {
        coordinator?.close()
    }

}
