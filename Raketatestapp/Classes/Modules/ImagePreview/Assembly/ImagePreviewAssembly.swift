//
//  ImagePreviewAssembly.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 11.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

final class ImagePreviewAssembly: Assembly {

    private weak var coordinator: ImagePreviewCoordinating?
    private let imageURL: URL

    init(coordinator: ImagePreviewCoordinating,
         imageURL: URL) {
        self.coordinator = coordinator
        self.imageURL = imageURL
    }

    func assemble() -> View {
        let view: ImagePreviewViewController = .instantiate(with: .imagePreview)
        let presenter = ImagePreviewPresenter(coordinator: coordinator,
                                              imageURL: imageURL)

        presenter.view = view
        view.presenter = presenter

        return view
    }

}
