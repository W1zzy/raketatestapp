//
//  TopListPresenter.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

typealias LoadTopCompletion = () -> Void

protocol TopListCoordinating: class {

    func openPreview(for url: URL)
}

protocol TopListPresenting: Presenter, RefreshControlPresenting {

    func loadNextPage()
    func itemSelected(at index: Int)

}

class TopListPresenter {

    private struct Constants {
        static let defaultLimit = 20
    }

    // MARK: Injected

    weak var view: TopListViewInput?
    private weak var coordinator: TopListCoordinating?
    private let networking: Networking

    // MARK: Properties

    private var items: [TopListItemResponseModel] = []
    private var pageModel = PageModel(with: Constants.defaultLimit)

    init(coordinator: TopListCoordinating?,
         networking: Networking) {
        self.coordinator = coordinator
        self.networking = networking
    }

}

extension TopListPresenter: TopListPresenting {

    func viewLoaded() {
        loadTop()
    }

    func loadNextPage() {
        loadTop()
    }

    func itemSelected(at index: Int) {
        guard items.count > index else { return }
        let item = items[index]

        guard let type = item.type else { return }
        switch type {
        case .link:
            view?.openURL(item.url)
        case .image:
            guard let url = item.url else { return }
            coordinator?.openPreview(for: url)
        }
    }

}

// MARK: Pagination

extension TopListPresenter {

    func refresh(with completion: @escaping RefreshCompletion) {
        items = []
        pageModel.first()
        loadTop(with: completion)
    }

}

private extension TopListPresenter {

    func loadTop(with completion: LoadTopCompletion? = nil) {
        networking.sendRequest(ListingsEndpoint.getTop(page: pageModel),
                               response: TopListResponseModel.self) { [weak self] result in
            switch result {
            case .success(let topList):
                self?.items.append(contentsOf: topList.items)
                let viewModels = topList.items.map { TopListItemTableViewCell.Model(with: $0) }
                if viewModels.count == self?.pageModel.limit {
                    self?.view?.unblockPagination()
                }
                if self?.pageModel.isFirst == true {
                    self?.view?.updateTopListItems(viewModels)
                } else {
                    self?.view?.appendTopListItems(viewModels)
                }
                self?.pageModel = topList.page
            case .failure(let error):
                self?.view?.showError(error)
            }
            completion?()
        }
    }

}
