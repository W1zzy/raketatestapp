//
//  TopListAssembly.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

final class TopListAssembly: Assembly {

    private weak var coordinator: TopListCoordinating?
    private let networking: Networking

    init(coordinator: TopListCoordinating,
         networking: Networking) {
        self.coordinator = coordinator
        self.networking = networking
    }

    func assemble() -> View {
        let view: TopListViewController = .instantiate(with: .topList)
        let presenter = TopListPresenter(coordinator: coordinator,
                                         networking: networking)

        presenter.view = view
        view.presenter = presenter

        return view
    }

}
