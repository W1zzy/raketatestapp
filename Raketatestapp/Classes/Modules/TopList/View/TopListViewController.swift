//
//  TopListViewController.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 04.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

protocol TopListViewInput: View {

    func showError(_ error: Error)
    func unblockPagination()
    func updateTopListItems(_ items: [TopListItemTableViewCell.Model])
    func appendTopListItems(_ items: [TopListItemTableViewCell.Model])

}

class TopListViewController: UIViewController {

    // MARK: Injected

    var presenter: TopListPresenting?

    // MARK: Pagination interface

    var infinityScrollBlocked: Bool = false

    // MARK: Outlets

    @IBOutlet private weak var tableView: UITableView!

    // MARK: Properties

    private lazy var activityIndicator = UIActivityIndicatorView.buildActivityIndicator(with: .gray)

    // MARK: Private

    private var dataSource = ArrayTableViewDataSource<TopListItemTableViewCell.Model, TopListItemTableViewCell>()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTableView()
        setupActivityIndicator()
        presenter?.viewLoaded()
    }
    
}

// MARK: TopListViewInput

extension TopListViewController: TopListViewInput {

    func showError(_ error: Error) {
        activityIndicator.stopAnimating()
        presentError(error)
    }

    func unblockPagination() {
        infinityScrollBlocked = false
    }

    func updateTopListItems(_ items: [TopListItemTableViewCell.Model]) {
        activityIndicator.stopAnimating()
        dataSource.updateWith(items: items)
        tableView.reloadData()
    }

    func appendTopListItems(_ items: [TopListItemTableViewCell.Model]) {
        dataSource.append(items: items)
        tableView.reloadData()
    }

}

// MARK: UITableViewDelegate

extension TopListViewController: UITableViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewScrolled(scrollView)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.itemSelected(at: indexPath.row)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

// MARK: Pagination

extension TopListViewController: ViewPaginatable {

    var paginationType: PaginationType {
        .vertical
    }

    func scrollViewDidScrollOverThreshold() {
        infinityScrollBlocked = true
        presenter?.loadNextPage()
    }

}

// MARK: Helpers

private extension TopListViewController {

    func setupActivityIndicator() {
        tableView.addSubview(activityIndicator)
        activityIndicator.centerOnView(tableView)
        activityIndicator.startAnimating()
    }

    func prepareTableView() {
        TopListItemTableViewCell.registerFor(tableView: tableView)
        tableView.dataSource = dataSource
        tableView.delegate = self

        guard let presenter = presenter else { return }
        addRefreshControl(to: tableView, with: presenter)
    }

}
