//
//  TopListItemTableViewCell.swift
//  Raketatestapp
//
//  Created by Антон Братчик on 05.08.2020.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class TopListItemTableViewCell: UITableViewCell {

    struct Model: Equatable {
        let title: String
        let author: String
        let date: Date
        let commentsCount: Int
        let thumbnailURL: URL?

        init(with model: TopListItemResponseModel) {
            title = model.title
            author = model.author
            date = model.created
            commentsCount = model.commentsCount
            thumbnailURL = model.thumbnail
        }
    }

    // MARK: Outlets

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var postedDateLabel: UILabel!
    @IBOutlet private weak var commentsCountLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.isHidden = false
        thumbnailImageView.image = nil
    }
}

extension TopListItemTableViewCell: Configurable {

    func configure(with model: TopListItemTableViewCell.Model) {
        titleLabel.text = model.title
        authorLabel.text = model.author
        commentsCountLabel.text = "\(model.commentsCount) comments"

        if let url = model.thumbnailURL {
            thumbnailImageView.setImage(with: url)
        } else {
            thumbnailImageView.isHidden = true
        }

        let components = Calendar.current.dateComponents([.hour],
                                                         from: model.date,
                                                         to: Date())
        if let hours = components.hour {
            postedDateLabel.text = "\(hours) hours ago"
        } else {
            postedDateLabel.text = nil
        }
    }

}
